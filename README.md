# blog-bitbucket-pipelines-automated-tests

Example repository demonstrating using Bitbucket Pipelines
to automate testing on pull requests and merges. See the
blog post for more details:

https://davistobias.com/articles/bitbucket-pipelines-automated-tests/

## Project setup:

Clone the repository to your local machine:

```bash
git clone https://saibotsivad@bitbucket.org/saibotsivad/blog-bitbucket-pipelines-automated-tests.git
cd blog-bitbucket-pipelines-automated-tests
```

Install all dependencies (requires [npm]() to be installed):

```bash
npm install
```

Make sure tests pass:

```bash
npm run lint
npm run test
```

## License

The contents of this repository are published under the
[Very Open License](http://veryopenlicense.com).
