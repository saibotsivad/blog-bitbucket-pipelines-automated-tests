/**
 * Simple demo code that takes an input and, if it's
 * a string, turns it to lower cased letters.
*/
module.exports = input => {
	if (typeof input === 'string') {
		return input.toLowerCase()
	}
	return input
}
