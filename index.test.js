const test = require('tape')
const lower = require('./index.js')

test('simple demo code to show what tests look like', t => {
	t.equal(lower('mIxEd'), 'mixed', 'lowercases strings')
	t.equal(lower(3), 3, 'does nothing with non-strings')
	t.equal(lower(), undefined, 'does nothing if not defined')
	t.end()
})
